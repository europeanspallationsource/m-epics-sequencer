PROJECT=seq
include ${EPICS_ENV_PATH}/module.Makefile

SEQ_VER=2.1.10
SEQ_SRC_PATH=seq-${SEQ_VER}/src

TEMPLATES = -none-
DBDS = ${SEQ_SRC_PATH}/dev/devSequencer.dbd

HEADERS += $(addprefix ${SEQ_SRC_PATH}/,pv/pv.h pv/pvAlarm.h pv/pvType.h seq/seqCom.h) seq_release.h

devSequencer.o: seq_release.h

SOURCES += $(wildcard $(addprefix ${SEQ_SRC_PATH}/, pv/pvNew.cc pv/pv.cc pv/pvCa.cc seq/seq_*.c dev/*.c))

EXECUTABLES += snc
STARTUPS = -none-

vpath %.c ../../${SEQ_SRC_PATH}/snc
vpath %.h ../../${SEQ_SRC_PATH}/snc

ifeq (${EPICSVERSION},3.14)
O.snc/%.o: %.c
	${QUIET}${MKDIR} -p $(dir $@)
	$(COMPILE.c) -o $@ $<

O.snc/snl.o: O.snc/snl.c
	${QUIET}${MKDIR} -p $(dir $@)
	$(COMPILE.c) -o $@ $<
else
O.snc/%.o: %.c
	${QUIET}${MKDIR} -p $(dir $@)
	$(COMPILE.c) -c -o $@ $<

O.snc/snl.o: O.snc/snl.c
	${QUIET}${MKDIR} -p $(dir $@)
	$(COMPILE.c) -c -o $@ $<
endif

USR_INCLUDES += -I../../${SEQ_SRC_PATH}/snc -IO.snc
USR_CPPFLAGS += -DPVCA

${BUILD_PATH}/${EPICSVERSION}/bin/${T_A}/snc: $(addprefix O.snc/,$(patsubst %.c,%.o,snl.c main.c expr.c var_types.c analysis.c gen_code.c gen_ss_code.c gen_tables.c sym_table.c builtin.c lexer.c))
	${QUIET}${MKDIR} -p $(dir $@)
	#$(LINK.cpp) $(filter %.o, $^)
	$(CCC) -o $@ -L ${EPICS_BASE_LIB} -Wl,-rpath,${EPICS_BASE_LIB} -lCom $(filter %.o, $^)

lexer.c: ../../${SEQ_SRC_PATH}/snc/snl.re O.snc/snl.h
	re2c -s -b -o $@ $<

lemon: ../../${SEQ_SRC_PATH}/lemon/lemon.c
	gcc -o $@ $^

O.snc/snl.c O.snc/snl.h: $(addprefix ../../${SEQ_SRC_PATH}/snc/, snl.lem snl.lt) lemon
	${QUIET}${MKDIR} -p $(dir $@)
	$(RM) O.snc/snl.c O.snc/snl.h
	./lemon o=O.snc $<

seq_release.h:
	$(RM) $@
	$(PERL) ../../${SEQ_SRC_PATH}/seq/seq_release.pl ${SEQ_VER} > $@
