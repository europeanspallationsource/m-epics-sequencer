#+======================================================================
# $HeadURL: https://svnpub.iter.org/codac/iter/codac/dev/units/m-epics-sequencer/tags/CODAC-CORE-4.3.0/epics-sequencer.spec $
# $Id: epics-sequencer.spec 44723 2014-02-18 13:31:22Z isaevs $
#
# Project       : CODAC Core System
#
# Description   : Spec file
#
# Author(s)     : Cosylab
#
# Copyright (c) : 2010-2014 ITER Organization,
#                 CS 90 046
#                 13067 St. Paul-lez-Durance Cedex
#                 France
#
# This file is part of ITER CODAC software.
# For the terms and conditions of redistribution or use of this software
# refer to the file ITER-LICENSE.TXT located in the top level directory
# of the distribution package.
#
#-======================================================================
%define install_dir %{epics_modules}/sncseq

Name:           %{codac_rpm_prefix}-epics-sequencer%{?codac_patch_postfix}
Summary:        State Notation Compiler / Sequencer binary
Version:        %{codac_version_full}.v%{unit_version_full}
Release:        %{codac_packaging_release}
Packager:       ITER Organization
Vendor:         Los Alamos National Laboratory
License:        -
Group:          Development/CODAC
URL:            http://epics.web.psi.ch/software/sequencer/

Source0:        seq-%{unit_version_full}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{unit_version_full}-%{release}-root-%(%{__id_u} -n)

%provides_self

BuildRequires:  %{codac_rpm_prefix}-epics-devel
BuildRequires:	%{codac_rpm_prefix}-common-devel
BuildRequires:	re2c

%requires_current %{codac_rpm_prefix}-epics
%requires_current %{codac_rpm_prefix}-common

# postun is implied by Requires anyways
#Requires(postun): %{codac_rpm_prefix}-common

AutoReq:        no


%description
EPICS Sequencer module, which provides a state notation compiler ("snc") and
run-time support ("seq") for implementing state transition diagrams in an
EPICS environment.


%prep
%setup -n seq-%{unit_version_full}
echo "EPICS_BASE=%{epics_base}" > configure/RELEASE


%build
export EPICS_HOST_ARCH=%{epics_host_arch}
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
install -d %{buildroot}%{install_dir}
install -d %{buildroot}%{install_dir}/bin
install -d %{buildroot}%{install_dir}/bin/%{epics_host_arch}
install -d %{buildroot}%{install_dir}
install -d %{buildroot}%{install_dir}/lib
install -d %{buildroot}%{install_dir}/lib/%{epics_host_arch}

cp bin/%{epics_host_arch}/snc %{buildroot}%{install_dir}/bin/%{epics_host_arch}
cp lib/%{epics_host_arch}/libpv.so %{buildroot}%{install_dir}/lib/%{epics_host_arch}
cp lib/%{epics_host_arch}/libseq.so %{buildroot}%{install_dir}/lib/%{epics_host_arch}
cp -r configure %{buildroot}%{install_dir}
cp -r include %{buildroot}%{install_dir}

install -d %{buildroot}%{codac_alt}
#list of slaves for alternatives system
for file in `ls -1 %{buildroot}/%{install_dir}/bin/%{epics_host_arch}/*`
do
  	file=`basename $file`
        echo "--slave %{_bindir}/$file codac-usr-$file %{install_dir}/bin/%{epics_host_arch}/$file \\" >> %{buildroot}%{codac_alt}/sequencer
done


%posttrans
%beginlog_posttrans
sh %{codac_bin}/codacalt
%endlog


%postun
%beginlog_postun
# On uninstall, update alternatives
[[ $1 == 0 && -f %{codac_bin}/codacalt ]] && sh %{codac_bin}/codacalt
%endlog


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{codac_root}
%dir %{codac_conf}
%dir %{codac_alt}
%config %{codac_alt}/sequencer


%changelog
* Fri Feb 19 2010 Takashi Nakamoto <takashi.nakamoto@cosylab.com>
- Clean up and added recommended information in the spec file. (Bug #292)
* Fri Jan 8 2010 Janez Golob <janez.golob@cosylab.com>
- Initial version.
